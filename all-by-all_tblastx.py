import sys,os

"""
input: a fasta file of DNA
output: all-by-all blastn results in customized tabular format
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python all-by-all_blastn.py fastafile num_cores"
		sys.exit(0)
	
	infile = sys.argv[1]
	num_cores = sys.argv[2]

	print "conducting blast on "+infile
	#create blast database
	cmd = "makeblastdb -in "+infile+" -parse_seqids -dbtype nucl -out "+infile+".db"
	print cmd
	os.system(cmd)
	#blast against the database
	cmd = "time tblastx -db "+infile+".db -query "+infile
	cmd += " -evalue 0.00001 -num_threads "+num_cores+" -max_target_seqs 100 -out "+infile+".rawtblastx "
	cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
	print cmd
	os.system(cmd)
	print "blast is done"

