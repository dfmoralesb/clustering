import os,sys
import tree_utils
import phylo3,newick3

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python homolog_to_MUL_tree.py incladeDIR outDIR"
		sys.exit(0)
		
	inDIR,outDIR = sys.argv[1:]
	if inDIR[-1] != "/": inDIR += "/"
	if outDIR[-1] != "/": outDIR += "/"
	for i in os.listdir(inDIR):
		print i
		with open(inDIR+i,"r") as infile:
			tree = newick3.parse(infile.readline())
		for leaf in tree.leaves():
			leaf.label = tree_utils.get_name(leaf.label)
		names = tree_utils.get_front_names(tree)
		with open(outDIR+i+".MUL","w") as outfile:
			outfile.write(newick3.tostring(tree)+";\n")
