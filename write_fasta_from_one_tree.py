import sys,os,newick3
from Bio import SeqIO

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: write_fasta_from_one_tree.py fasta tree"
		sys.exit()

	handle = open(sys.argv[1])
	seqDICT = {} #key is seqid, value is seq
	for record in SeqIO.parse(handle,"fasta"):
		seqDICT[str(record.id)] = str(record.seq)
	handle.close()
	
	with open(sys.argv[2],"r") as infile:
		intree = newick3.parse(infile.readline())
	seqids = get_front_labels(intree)
	
	with open(sys.argv[2]+".fa","w") as outfile:
		for seqid in seqids:
			print seqid
			try:
				outfile.write(">"+seqid+"\n"+seqDICT[seqid]+"\n")
			except:
				print seqid,"not found in fasta file"
