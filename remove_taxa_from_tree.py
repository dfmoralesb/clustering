import sys,os
import newick3,phylo3

#remove = ["JBGU","HTDC","QAIR","HSXO"]
remove = ["Pseu"]
#file_end = ".tre"
file_end = ".phy"

#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.replace("_R_","")[:4]

#smooth the kink created by prunning
#to prevent creating orphaned tips after prunning twice at the same node
def remove_kink(node,curroot):
	if node == curroot and curroot.nchildren == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: curroot = phylo3.reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: 100taxa_to_96taxa.py 100DIR 96DIR"
		sys.exit()
	
	inDIR = sys.argv[1]+"/"
	outDIR = sys.argv[2]+"/"
	
	for treefile in os.listdir(inDIR):
		if treefile[-len(file_end):] == file_end:
			with open(inDIR+treefile,"r") as infile: #only 1 tree in each file
				intree = newick3.parse(infile.readline())
			root = intree
			for leaf in intree.leaves():
				if get_name(leaf.label) in remove:
					print treefile,leaf.label
					node = leaf.prune()
					node,root = remove_kink(node,root)
			with open(outDIR+treefile,"w") as outfile:
				outfile.write(newick3.tostring(root)+";\n")
			
