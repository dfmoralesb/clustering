"""
Read initial concatenated fasta before clustering

Since no taxon repeats
Shorten seq id to taxon id
"""

import sys,os,newick3,phylo3
from Bio import SeqIO
from tree_utils import *

ORTHO_TREE_FILE_ENDING = ".tre"


if __name__ =="__main__":
	if len(sys.argv) != 5:
		print "usage: python write_ortholog_fasta_files.py fasta treeDIR outDIR MIN_TAXA"
		sys.exit()
	
	fasta = sys.argv[1]
	treDIR = sys.argv[2]+"/"
	outDIR = sys.argv[3]+"/"
	MIN_TAXA = int(sys.argv[4])

	print "Reading the original fasta file"
	handle = open(fasta,"rU")
	seqDICT = {} #key is taxonID, value is seq
	for s in SeqIO.parse(handle,"fasta"):
		seqID,seq = str(s.id),str(s.seq)
		taxonID = get_name(seqID)
		seqDICT[seqID] = seq
	handle.close()
	print len(seqDICT), "sequences read"
	
	print "Writing fasta files"
	for i in os.listdir(treDIR):
		if i.endswith(ORTHO_TREE_FILE_ENDING):
			#read in tree tips and write output alignment
			with open(treDIR+i,"r")as infile:
				intree = newick3.parse(infile.readline())
			print i
			labels = get_front_labels(intree)
			if len(labels) >= MIN_TAXA:
				with open(outDIR+i.replace(ORTHO_TREE_FILE_ENDING,".fa"),"w") as outfile:
					for lab in labels:
						name = get_name(lab)
						outfile.write(">"+name+"\n"+seqDICT[lab]+"\n")

