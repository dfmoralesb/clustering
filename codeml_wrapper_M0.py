"""
All dir need to have absolute path
"""

import newick3,phylo3,sys,os
from Bio.Phylo.PAML import codeml

if __name__ =="__main__":
	if len(sys.argv) != 5:
		print "usage: codeml_wrapper.py alnDIR treDIR outDIR doneDIR"
		sys.exit()
	
	alnDIR = sys.argv[1]+"/"
	treDIR = sys.argv[2]+"/"
	outDIR = sys.argv[3]+"/"
	doneDIR = sys.argv[4]+"/"
	
	done = [] #store names that are done
	for j in os.listdir(doneDIR):
		done.append(j.split(".cary")[0])
	print len(done),"files done"
		
	for i in os.listdir(treDIR):
		if "best" not in i:	continue
		name = i.replace("RAxML_bestTree.","")
		name = name.replace(".fasta.phy","")
		if name.split(".cary")[0] in done:
			print name,"is done"
			continue
		print name
		workDIR = outDIR+name+".codemlDIR/"
		cmd = "mkdir "+workDIR
		os.system(cmd)
		
		cml = codeml.Codeml()
		cml.alignment = alnDIR+name+".pal2nal"
		cml.tree = treDIR+i
		cml.out_file= workDIR+name+".codemlout"
		cml.working_dir = workDIR		
		cml.set_options(noisy = 1)
		cml.set_options(verbose = 0)
		cml.set_options(runmode = 0)
		cml.set_options(model = 0)
		cml.set_options(NSsites = [0])
		cml.set_options(icode = 0)	
		cml.set_options(seqtype = 1)
		cml.set_options(CodonFreq = 2) #F3x4
		cml.set_options(clock = 0)
		cml.set_options(cleandata = 0)
		cml.set_options(fix_blength = 2) #fixed
		cml.set_options(fix_kappa = 0) #to be estimated
		cml.set_options(kappa = 1.5)
		cml.set_options(fix_alpha = 1)
		cml.set_options(alpha = 0)
		cml.set_options(fix_omega = 0)
		cml.set_options(omega = 0.2)
		cml.set_options(ncatG = 10)
		cml.set_options(Small_Diff = 5e-6)
		cml.set_options(method = 1)
		cml.set_options(RateAncestor = 0)
		cml.set_options(getSE = 0)
		cml.set_options(Mgene = 0)
		cml.run(verbose=True)
