import newick3,phylo3,os,sys
from tree_utils import *

def get_clades_for_phypart(indir,min_taxa,outdir):
	if indir[-1] != "/": indir += "/"
	if outdir[-1] != "/": outdir += "/"
	for i in os.listdir(indir):
		with open(indir+i,"r") as infile: #only 1 tree in each file
			intree = newick3.parse(infile.readline())
		names = get_front_names(intree)
		if len(set(names)) >= int(min_taxa):
			print i
			with open(indir+i,"r") as infile:
				tree = newick3.parse(infile.readline())
			for node in tree.iternodes():
				node.length = None # remove branch length
				if node.istip: # replace with taxon id
					node.label = get_name(node.label)
				tree.label = None
			with open(outdir+i+".phypart","w") as outfile:
				outfile.write(newick3.tostring(tree)+";\n")

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python prepare_inclades_for_phypart.py incladedir minimal_taxa outdir"
		sys.exit(0)
	
	indir,min_taxa,outdir = sys.argv[1:]
	get_clades_for_phypart(indir,min_taxa,outdir)

