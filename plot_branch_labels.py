import newick3,phylo3,os,sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python plot_branch_labels.py treefile"
		sys.exit(0)

	with open(sys.argv[1],"r") as infile:
		intree = newick3.parse(infile.readline())
	outname = sys.argv[1]+".branch_labels"
	outfile = open(outname,"w")
	for node in intree.iternodes():
		if node.istip or node == intree or node.label == None:
			continue
		print node.label
		outfile.write(node.label+"\n")						
	outfile.close()
	print "output written to", outname
