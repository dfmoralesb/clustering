import newick3,phylo3,os,sys
import tree_utils

if __name__ =="__main__":
	if len(sys.argv) != 2:
		print "usage: python check_names.py indir"
		sys.exit()
		
	indir = sys.argv[1]
	for i in os.listdir(indir):
		with open(indir+"/"+i,"r") as infile:
			intree = newick3.parse(infile.readline())
		for node in intree.iternodes(): # walk through nodes
			if node == intree:
				if node.nchildren != 3:
					print i
					print "root clades:",node.nchildren
			else:
				if node.nchildren != 2 and not node.istip:
					print i
					print node.nchildren
					print tree_utils.get_front_labels(node)
	
	
