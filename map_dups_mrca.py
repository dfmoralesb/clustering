"""
map ratio of #dup/#observe from extracted clades
to the rooted species tree

1. Do bootstrap on homolog trees and keep the gene trees with bootstrap
   support values as branch labels
2. Use extract_clades.py to remove outgroups and root orthogroups
3. Root the species tree in figtree, and save as a newick file. Do not 
   remove outgroups on the specie tree


filtered by average bootstrap support in each gene tree
Change the value of BOOT_FILTER accordingly
"""

import phylo3,newick3,os,sys,math
import tree_utils

bootfilters=[50]

def pass_boot_filter(node,bootfilter):
	total = 0.0
	count = 0
	for i in node.iternodes():
		if not i.istip and i.parent != None:
			total += float(i.label)
			count += 1
	if count == 0: #extracted clades with only two tips
		return True
	boot_average = total / float(count)
	print "Average bootstrap support:", boot_average
	return boot_average >= float(bootfilter)
	

def map_dups(DIR,sptree,nodeDICT,min_taxa,bootfilter):
	count = 0 #record number of trees that pass BOOT_FILTER
	for treefile in os.listdir(DIR):
		print treefile
		with open(DIR+treefile,"r") as infile:
			gentree = newick3.parse(infile.readline()) #one tree only
		num_taxa = len(set(tree_utils.get_front_names(gentree)))
		print "Number of taxa:",num_taxa
		if num_taxa < int(min_taxa):
			print "failed min_taxa filter"
			continue
		if not pass_boot_filter(gentree,bootfilter):
			print "failed bootstrap"
			continue
		dup_nodes = set() #store unique nodes that need to be counted
		#this way no node in nested dup will be counted multiple times
		count += 1
		for gennode in gentree.iternodes():
			if gennode.nchildren != 2: continue #skip tip, but check the root
			child0 = gennode.children[0]
			child1 = gennode.children[1]
			if not child0.istip and not child1.istip:
				names0 = set(tree_utils.get_front_names(child0))
				names1 = set(tree_utils.get_front_names(child1))
				if len(set(names0).intersection(names1)) >= 2:
					genfront = set(tree_utils.get_front_names(gennode))
					mrca = phylo3.getMRCA(genfront,sptree)
					print genfront
					print tree_utils.get_front_labels(mrca)
					print
					dup_nodes.add(mrca) #dup found
		#summarize all the dups found in this extracted clade
		#when nested dups found, only count once per extracted clade
		for spnode in dup_nodes:
			if spnode not in nodeDICT:
				nodeDICT[spnode] = 0
			nodeDICT[spnode] += 1
	return count,nodeDICT
					
if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python map_dups.py incladeDIR rooted_spTree min_taxa outname"
		sys.exit(0)
		
	indir,sptreefile,min_taxa,outname = sys.argv[1:]
	if indir[-1] != "/": indir += "/"
	with open(sptreefile,"r") as infile:
		sptree = newick3.parse(infile.readline())
		
	for bootfilter in bootfilters:
		nodeDICT = {} #key is internal nodes of the species tree, value is dup
		for spnode in sptree.iternodes():
			if not spnode.istip and not spnode.parent == None:
				spnode.label = False #no dup
		count,nodeDICT = map_dups(indir,sptree,nodeDICT,min_taxa,bootfilter)
	
		#output a species tree with counts of duplications
		count_out = "dup_count_filter"+str(bootfilter).split(".")[0]+"_global."+outname
		for spnode in sptree.iternodes():
			if not spnode.istip:
				if spnode in nodeDICT:
					spnode.label = str(nodeDICT[spnode])+"/"+str(count)
				else: spnode.label = None
		with open(count_out,"w") as outfile:
			outfile.write(newick3.tostring(sptree)+";\n")
			
		#output another species tree with proportion of duplications
		perc_out = count_out.replace("count","perc")
		for spnode in sptree.iternodes():
			if not spnode.istip:
				if spnode in nodeDICT:
					spnode.label = nodeDICT[spnode] / float(count)
				else: spnode.label = None
		with open(perc_out,"w") as outfile:
			outfile.write(newick3.tostring(sptree)+";\n")
		print "Output trees written to"
		print count_out
		print perc_out


