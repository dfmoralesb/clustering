"""
re-implimentation of mcl by stephen
"""

import numpy as np
import math
import copy

def inflate(mat,inflation):
	for i in range(len(mat)):
		for j in range(len(mat)):
			mat[j,i] = math.pow(mat[j,i],inflation)
	mat = mat/sum(mat)
	return mat

def testeq(a,b):
	sdiff = 0
	for i in range(len(a)):
		for j in range(len(a)):
			sdiff += a[j,i]-b[j,i]
	if abs(sdiff) < 1e-100:
		return True
	else:
		return False

#diagonal could be 1 or not
a = np.matrix([[1.,1.,1.,1.],[1.,1.,0.,1.],[1.,0.,1.,0.],[1.,1.,0.,1.]])
#scale the matrix
a = a/sum(a)
#calculate the e
a = pow(a,2)
#calculate the inflation
inflation = 2	
maxiter = 100
iter = 0
going = True
while going:
	olda = np.matrix(a)
	a = inflate(a,inflation)
	if testeq(a,olda) == True:
		going = False
	iter += 1
	if iter > maxiter:
		print "hit max iter"
		break
print a
#determine the clusters
clusters = {}
for i in range(len(a)):
	clusters[i] = []
	for j in range(len(a)):
		if a[i,j] > 0:
			clusters[i].append(j)

print clusters

