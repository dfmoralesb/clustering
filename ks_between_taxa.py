"""
make sure that cd-hit is in the path and is executable
make sure that synonymous_calc.py and bin from
the kaks-calculator https://code.google.com/p/kaks-calculator/
are in the current directory.

create a directory with four files only:
taxonID1.pep.fa
taxonID1.cds.fa
taxonID2.pep.fa
taxonID2.cds.fa

Create a tabular file that each line contains
code	taxon_name
separated by tab
"""

import sys,os
from Bio import SeqIO
import newick3,phylo3
import tree_utils
from collections import Counter

max_ks_to_plot = 3.0


def get_output_name(taxon_name):
	"""reformat taxon name for output.
	Modify it according to the output format needed"""
	spls = taxon_name.split("_")
	return spls[1]+" "+spls[2].split("=")[0] # genus species

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python ks_between_taxa.py workingDIR taxonID1 taxonID2 num_cores taxon_name_table"
		sys.exit(0)
	
	workingdir,taxon1,taxon2,num_cores,taxon_table = sys.argv[1:]
	if workingdir[-1] != "/": workingdir += "/"

	rawpep1 = workingdir+taxon1+".pep.fa"
	rawpep2 = workingdir+taxon2+".pep.fa"
	pep1 = workingdir+taxon1+".cd-hit"
	pep2 = workingdir+taxon2+".cd-hit"
	cds1 = workingdir+taxon1+".cds.fa"
	cds2 = workingdir+taxon2+".cds.fa"
	out  = workingdir+taxon1+"_"+taxon2
		
	if not os.path.exists(pep1):
		cmd = "cd-hit -i "+rawpep1+" -o "+pep1+" -c 0.99 -n 5 -T "+str(num_cores)
		print cmd
		os.system(cmd)
	if not os.path.exists(pep1):
		cmd = "cd-hit -i "+rawpep2+" -o "+pep2+" -c 0.99 -n 5 -T "+str(num_cores)
		print cmd
		os.system(cmd)

	# blastp and take top hit from both direction
	if not os.path.exists(pep1+".rawblastp"):
		cmd = "makeblastdb -in "+pep2+" -parse_seqids -dbtype prot -out "+pep2
		print cmd
		os.system(cmd)
		cmd = "blastp -db "+pep2+" -query "+pep1+" -evalue 10 -num_threads "+str(num_cores)
		cmd += " -max_target_seqs 1 -out "+pep1+".rawblastp "
		cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
		print cmd
		os.system(cmd)
		os.system("rm "+pep2+".p*")
	if not os.path.exists(pep2+".rawblastp"):
		cmd = "makeblastdb -in "+pep1+" -parse_seqids -dbtype prot -out "+pep1
		print cmd
		os.system(cmd)
		cmd = "blastp -db "+pep1+" -query "+pep2+" -evalue 10 -num_threads "+str(num_cores)
		cmd += " -max_target_seqs 1 -out "+pep2+".rawblastp "
		cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
		print cmd
		os.system(cmd)
		os.system("rm "+pep2+".p*")

	#filter for reciprocol best hits with sufficient pident and nident
	if not os.path.exists(taxon1+"_"+taxon2+"_RBH"):
		taxon1_hits = set() #tubles of hits that pass filter
		infile = open(pep1+".rawblastp","r")
		line_count = 0
		tuple_count = 0
		for line in infile:
			spls = line.split("\t")
			if len(spls) < 3: continue
			line_count += 1
			query,hit,pident,nident = spls[0],spls[2],float(spls[5]),int(spls[6])
			if pident < 20.0 or nident < 50: continue
			tuple_count += 1
			taxon1_hits.add((query,hit))
		infile.close()
		print taxon1,line_count,"lines read,",tuple_count,"hits added"
		
		taxon2_hits = set() #tubles of hits that pass filter
		infile = open(pep2+".rawblastp","r")
		line_count = 0
		tuple_count = 0
		for line in infile:
			spls = line.split("\t")
			if len(spls) < 3: continue
			line_count += 1
			query,hit,pident,nident = spls[0],spls[2],float(spls[5]),int(spls[6])
			if pident < 20.0 or nident < 50: continue
			tuple_count += 1
			taxon2_hits.add((hit,query)) #reverse order to match pairs
		infile.close()
		print taxon2,line_count,"lines read,",tuple_count,"hits added"
		
		rbh = taxon1_hits & taxon2_hits
		with open(out+"_RBH","w") as outfile:
			for hit in rbh:
				outfile.write(hit[0]+"\t"+hit[1]+"\n")

	# write out cds pairs and pep pairs
	if not os.path.exists(taxon1+"_"+taxon2+".cds.pairs.fa"):
		cdsDICT = {} #key is seqid, value is seq
		with open(cds1,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				cdsDICT[str(record.id)] = str(record.seq)
		with open(cds2,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				cdsDICT[str(record.id)] = str(record.seq)
				
		pepDICT = {} #key is seqid, value is seq
		with open(pep1,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				pepDICT[str(record.id)] = str(record.seq)
		with open(pep2,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				pepDICT[str(record.id)] = str(record.seq)
				
		infile = open(out+"_RBH","r")
		outfile1 = open(out+".pep.pairs.fa","w")
		outfile2 = open(out+".cds.pairs.fa","w")
		for line in infile:
			seqid1,seqid2 = line.strip().split("\t")
			outfile1.write(">"+seqid1+"\n"+pepDICT[seqid1]+"\n")
			outfile1.write(">"+seqid2+"\n"+pepDICT[seqid2]+"\n\n")
			outfile2.write(">"+seqid1+"\n"+cdsDICT[seqid1]+"\n")
			outfile2.write(">"+seqid2+"\n"+cdsDICT[seqid2]+"\n\n")
		infile.close()
		outfile1.close()
		outfile2.close()

	if not os.path.exists(out+".ks"):
		cmd = "python synonymous_calc.py "+\
			  out+".pep.pairs.fa "+\
			  out+".cds.pairs.fa >"+\
			  out+".ks"
		print cmd
		os.system(cmd)
		#os.system("python report_ks.py "+taxon+".ks")

	#parse the ks output
	#0-name,1-dS-yn,2-dN-yn,3-dS-ng,4-dN-ng
	if not os.path.exists(out+".ks.yn") or not os.path.exists(out+".ks.ng"):
		first = True # marker to skip header
		infile = open(out+".ks","r")
		outfile1 = open(out+".ks.yn","w")
		outfile2 = open(out+".ks.ng","w")
		for line in infile:
			if first:
				first = False
				continue
			spls = line.strip().split(",")
			yn,ng = float(spls[1]),float(spls[3])
			if yn > 0.01 and yn < 3.0:
				outfile1.write(spls[1]+"\n")
			if ng > 0.01 and ng < 3.0:
				outfile2.write(spls[3]+"\n") 
		infile.close()
		outfile1.close()
		outfile2.close()
		
	with open(out+".R","w") as f: 
		f.write("pdf=pdf(file='"+out+".yn.pdf',width=7,height=7)\n")
		f.write("a<-read.table('"+out+".ks.yn')\n")
		f.write("hist(a[,1],breaks=60,col='grey',xlab='',ylab='',main='")
		f.write(taxon1+"_"+taxon2+"',axes=FALSE)\n")
		f.write("axis(1,pos=0)\naxis(2,pos=0)\ndev.off()\n\n")
