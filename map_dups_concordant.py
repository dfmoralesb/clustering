"""
Only map dup when bipartitions are concordant
all input gene trees and species trees have to be rooted.
Use the script extract_clades.py to remove outgroups and root gene trees
Open the species tree in figtree, reroot, and save as newick
Do not remove outgroups in the species tree

concordance criteria:
Find the node in species tree that is MRCA of all the front taxa in gene tree
Sister clade in the gene tree (gensister) has to be a subset of
sister clade in the species tree (spsister)
"""

import phylo3,newick3,os,sys
import tree_utils

def map_dups_concordant(DIR,sptree):
	mapDICT = {} # key is node, value is number of total biparts mapped
	dupDICT = {} # key is node, value is number of total biparts with dup mapped
	for treefile in os.listdir(DIR):
		print treefile
		with open(DIR+treefile,"r") as infile:
			gentree = newick3.parse(infile.readline()) #one tree only
		assert gentree.nchildren == 2, "root not bifurcating"
		map_nodes = set() #store unique spnodes that are observed in gene tree
		dup_nodes = set() #store unique spnodes that has duplication mapped
		
		for gennode in gentree.iternodes():
			if gennode.istip: continue # ignore tips
			assert gennode.nchildren == 2, "node not bifurcating"

			# concordant with species tree?
			genfront = set(tree_utils.get_front_names(gennode))
			if len(genfront) == 1:
				continue # sister tips of the same taxon
			mrca = phylo3.getMRCA(genfront,sptree)
			
			concordant = False
			if gennode == gentree or mrca == sptree: # root
				concordant = True # root always concordant
			else:
				gensister = set(tree_utils.get_front_names(gennode.get_sisters()[0]))
				spsister = set(tree_utils.get_front_names(mrca.get_sisters()[0]))
				# assume that species tree always has outgroups
				if gensister <= spsister:
					concordant = True
			if concordant:
				map_nodes.add(mrca)
				
				child0 = gennode.children[0]
				child1 = gennode.children[1]
				if child0.istip or child1.istip:
					continue # only look at deeper nodes
					# filter shape only after check for concordant
					# these non-dup occurance should not be ignored

				names0 = set(tree_utils.get_front_names(child0))
				names1 = set(tree_utils.get_front_names(child1))
				if len(names0 & names1) >= 2: # dup detected
					#print "mapping dup",names0 & names1
					dup_nodes.add(mrca)
		
		# summarize nodes mapped and nodes with dup detected
		print len(map_nodes),"nodes concordant, among which",len(dup_nodes),"duplications detected"
		for spnode in map_nodes:
			if spnode not in mapDICT:
				mapDICT[spnode] = 0
			mapDICT[spnode] += 1
		for spnode in dup_nodes:
			if spnode not in dupDICT:
				dupDICT[spnode] = 0
			dupDICT[spnode] += 1
	return mapDICT, dupDICT

					
if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python map_dups_concordant.py incladeDIR rooted_spTree outname"
		sys.exit(0)
		
	DIR,sptree,outname = sys.argv[1:]
	if DIR[-1] != "/": DIR += "/"
	with open(sptree,"r") as infile:
		sptree = newick3.parse(infile.readline())
	mapDICT,dupDICT = map_dups_concordant(DIR,sptree)
	
	#output a species tree with counts of duplications
	count_out = "dup_count_concord."+outname
	for spnode in sptree.iternodes():
		if not spnode.istip:
			if spnode in mapDICT:
				if spnode in dupDICT:
					spnode.label = str(dupDICT[spnode])+"/"+str(mapDICT[spnode])
				else: spnode.label ="0/"+str(mapDICT[spnode])
			else: spnode.label = None
	with open(count_out,"w") as outfile:
		outfile.write(newick3.tostring(sptree)+";\n")
		
	#output another species tree with proportion of duplications
	perc_out = count_out.replace("count","perc")
	for spnode in sptree.iternodes():
		if not spnode.istip:
			if spnode in mapDICT:
				if spnode in dupDICT:
					spnode.label = dupDICT[spnode] / float(mapDICT[spnode])
				else: spnode.label = 0.0
			else: spnode.label = None
	with open(perc_out,"w") as outfile:
		outfile.write(newick3.tostring(sptree)+";\n")
	print "Output trees written to"
	print count_out
	print perc_out
