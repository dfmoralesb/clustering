import sys,os

"""
use trimAl to trime alignment
then remove seqs that are very short or empty
"""

if len(sys.argv) != 3:
	print "python trimal_wrapper.py DIR occupancy(0.1 or 0.3)"
	sys.exit(0)
	
DIR = sys.argv[1]+"/"
occupancy = sys.argv[2]
#MIN_CHR = 150 #initial
MIN_CHR = 300
check = [] #store very skinny matrixes to check later
def count_info_sites(seq):
	seq = seq.replace("-","")
	seq_removen = seq.replace("n","")
	return len(seq_removen.replace("N",""))
	
for i in os.listdir(DIR):
	if i[-3:] != "aln": continue
	#com = "trimal -in "+DIR+i+" -out "+DIR+i+"-trim -gt "+occupancy+" -st 0.01"
	com = "trimal -in "+DIR+i+" -out "+DIR+i+"-trim -automated1"
	print com
	os.system(com)
		
	seqid = ""
	first = True
	infile = open(DIR+i+"-trim","r")
	outfile = open(DIR+i+"-cln","w")
	for line in infile:
		line = line.strip()
		if len(line) == 0: continue #skip empty lines
		if line[0] == ">":
			if seqid != "": #not at the first seqid
				if first == True:
					first = False
					length = len(seq)
					if length < 100: check.append(i+" "+str(length))
				num_informative_sites = count_info_sites(seq)
				if num_informative_sites >= MIN_CHR:
					outfile.write(">"+seqid+"\n"+seq+"\n")
			seqid,seq = line.split(" ")[0][1:],""
		else: seq += line.strip()
	#process the last seq
	num_informative_sites = count_info_sites(seq)
	if num_informative_sites >= MIN_CHR:
		outfile.write(">"+seqid+"\n"+seq+"\n")
	infile.close()
	outfile.close()	

print "check these matrixes"
for j in check: print j
